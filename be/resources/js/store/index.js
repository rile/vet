import { createStore } from 'vuex'

// Create a new store instance.
const store = createStore({
    modules: {

    },
    state () {
        return {
            user: null,
            languages: []
        }
    }
})

export default store;
