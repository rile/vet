require('./plugins/bootstrap');

import "bootstrap/scss/bootstrap.scss"
import "bootstrap"

import { createApp } from 'vue';
import { createI18n } from 'vue-i18n'
import '../../node_modules/bootstrap-icons/font/bootstrap-icons.scss';
import VueCal from 'vue-cal'
import 'vue-cal/dist/i18n/sr.js'
import 'vue-cal/dist/vuecal.css'

import VueToast from 'vue-toast-notification';
// Import one of the available themes
//import 'vue-toast-notification/dist/theme-default.scss';
// import 'vue-toast-notification/dist/theme-sugar.scss';
import 'vue-toast-notification/src/themes/sugar/index.scss';

import store from './store';
import privateRouter from './router/privateRouter';
import publicRouter from './router/publicRouter';
import App from './App.vue';
// import '../css/app.scss';

// Interceptor that redirects user to login page after his session is expired
axios.interceptors.response.use(null, error => {
    if (error.response && ([401, 403, 419].includes(error.response.status))) {
        localStorage.removeItem('access_token');
        window.location.href = '/';
    }
    return Promise.reject(error);
});

axios.defaults.baseURL = 'http://localhost/api/';
axios.defaults.withCredentials = true;
axios.defaults.headers.common["Accept"] = "application/json";
axios.defaults.headers.common["Content-type"] = "application/json";
axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('access_token')}`;
axios.defaults.headers.common['Accept-Language'] = localStorage.getItem('language');

const app = createApp(App)
app.use(store);
app.use(VueToast);
app.use(VueCal);
// app.use(BootstrapIconsPlugin);

const initRequests = [
    axios.get('init'),
    axios.get('http://localhost/sanctum/csrf-cookie')
];

axios.all(initRequests).then(axios.spread((...response) => {
    let responseData = response[0].data;
    // let csrf_token = response[1].config.headers['X-XSRF-TOKEN'];
    // console.log(csrf_token)

    // axios.defaults.headers.common['X-CSRF-TOKEN'] = csrf_token;
    const translationLanguages = Object.keys(responseData.translation)

    store.state.user = responseData.user
    store.state.languages = responseData.languages
    store.state.selected_language = translationLanguages[0]
    localStorage.setItem('language', translationLanguages[0])
    axios.defaults.headers.common['Accept-Language'] = translationLanguages[0];


    const i18n = createI18n({
        legacy: false,
        locale: translationLanguages[0], // set locale
        globalInjection: true,
        messages: responseData.translation, // set locale messages
    })

    setup(store.state.user !== null, i18n)
}));

function setup(loggedIn, i18n) {
    if (loggedIn) {
        app.use(privateRouter);
    } else {
        app.use(publicRouter);
    }
    app.use(i18n)

    app.mount('#app');
}

// function deleteAllCookies() {
//     var cookies = document.cookie.split(";");
//
//     for (var i = 0; i < cookies.length; i++) {
//         var cookie = cookies[i];
//         var eqPos = cookie.indexOf("=");
//         var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
//         document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
//     }
// }
