export default [
    {
        path: "/login",
        name: "login",
        component: () =>
            import('../../Views/Login.vue'),
        meta: {
            title: 'Login',
        },
    },
    {
        path: "/reset-password",
        name: "reset_password_request",
        component: () =>
            import('../../Views/Home.vue'),
        meta: {
            title: 'Reset Password',
        },
    },
    {
        path: "/reset-password/:token",
        name: "reset_password_change",
        component: () =>
            import('../../Views/Home.vue'),
        meta: {
            title: 'Reset Password',
        },
    }
];
