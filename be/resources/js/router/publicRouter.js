import * as VueRouter from 'vue-router';

import auth from "./public/auth";

const routes = [
    ...auth,
    {
        path: "/",
        name: "home",
        redirect: 'login'
    },
    {
        path: "/:catchAll(.*)", // Unrecognized path automatically matches 404
        redirect: '/login',
    }
]

const publicRouter = VueRouter.createRouter({
    history: VueRouter.createWebHistory(),
    routes,
});


export default publicRouter;
