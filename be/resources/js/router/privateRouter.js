import * as VueRouter from 'vue-router';
import user from './private/user'

const routes = [
    ...user,
    {
        path: "/",
        name: "home",
        component: () =>
            import('../Views/Home.vue'),
        props: true,
        meta: {
            title: 'Home',
            sidebar: true
        }
    },
    {
        path: '/404',
        name: "notFound",
        component: () =>
            import("../Views/NotFound.vue"),
        meta: {
            title: 'Not Found',
        }
    },
    {
        path: "/:catchAll(.*)", // Unrecognized path automatically matches 404
        redirect: '/404',
    }
];

const privateRouter = VueRouter.createRouter({
    history: VueRouter.createWebHistory(),
    routes,
});

export default privateRouter;
