export default [
    {
        path: "/user",
        name: "user",
        component: () =>
            import('../../Views/User/User.vue'),
        meta: {
            title: 'User',
            sidebar: true,
        },
    }
];
