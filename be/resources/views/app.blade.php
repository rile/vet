<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=0">

    <link rel="icon" href="/assets/favicon.ico" sizes="16x16">

    <title>Laravel</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
{{--    <style href="https://fonts.googleapis.com/css?family=Open+Sans"></style>--}}
    <link href="{{mix('/css/app.css')}}" rel="stylesheet" type="text/css">
</head>
<body class="background-blur">
<div id="app" class=""></div>
<script src="{{mix('/js/app.js')}}" ></script>
</body>
</html>
