<?php

return [
    'login' => [
        'username' => 'Email',
        'password' => 'Sifra',
        'forgotten_password' => 'Zaboravljena sifra',
        'login' => 'Uloguj se'
    ],
    'server' => [
        'error' => 'Dogodila se greska, molimo pokusajte ponovo, ili kontaktirajte administratora'
    ],
    'form' => [
        'required' => 'Obavezno polje',
        'bad_input' => 'Molimo proverite formu'
    ],
    'navigation' => [
        'home' => 'Pocetna',
        'profile' => 'Profil',
        'settings' => 'Podesavanja',
        'logout' => 'Izloguj se',
    ]
];
