<?php

return [
    'login' => [
        'username' => 'Email',
        'password' => 'Password',
        'forgotten_password' => 'Forgotten password',
        'login' => 'Log in'
    ],
    'server' => [
        'error' => 'There was an error, please try again or contact administrator'
    ],
    'form' => [
        'required' => 'Required field',
        'bad_input' => 'Please check form'
    ],
    'navigation' => [
        'home' => 'Home',
        'profile' => 'Profile',
        'settings' => 'Settings',
        'logout' => 'Logout',
    ]
];
