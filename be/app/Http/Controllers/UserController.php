<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class User extends BaseController
{
    public function createUser(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ], [
            'email.required' => 'form.required',
            'password.required' => 'form.required',
        ]);

        // Check validation failure
        if ($validator->fails()) {
            return response()->json([
                'message' => 'form.bad_input',
                'errors' => $validator->errors()
            ], 400);
        }

        $user = new \App\Models\User();
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->save();
    }
}
