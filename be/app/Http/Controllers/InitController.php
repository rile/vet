<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class InitController extends BaseController
{
    public function init(Request $request): JsonResponse
    {
        $language = $request->header('Accept-Language');
        if (!$language || $language === 'null') {
            $language = config('app.fallback_locale');
        }
        $translation = [$language => config('languages.'.$language)];
        $user = auth()->user();
        if ($user) {
            $user = [
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
            ];
        }

        return response()->json([
            'languages' => ['sr', 'en'],
            'user' => $user,
            'translation' => $translation,
            'config' => [
                'main_color' => '#6610f2'
            ]
        ]);
    }
}
