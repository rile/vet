<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
//        $credentials = $request->validate([
//            'email' => ['required'],
//            'password' => ['required'],
//        ]);
        $validator = Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required',
            ], [
                'email.required' => 'form.required',
                'password.required' => 'form.required',
            ]);

        // Check validation failure
        if ($validator->fails()) {
            return response()->json([
                'message' => 'form.bad_input',
                'errors' => $validator->errors()
            ], 400);
        }

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($credentials)) {
            $user = auth()->user();
            $returnData = [
                'errors' => [],
                'message' => 'login.logged_in',
                'data' => [
                    'access_token' => $user->createToken('access_token')->plainTextToken,
                ]
            ];

            return response()->json($returnData);
        }

        return response()->json([
            'errors' => [],
            'message' => "server.bad_request"],
            400
        );
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request): \Illuminate\Http\JsonResponse
    {
//        Auth::logout();
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return response()->json([
            'errors' => [],
            'message' => "server.success"],
            200
        );
    }
}
