<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('app');
//});
Route::group(['middleware' => ['web']], function () {
    Route::get('/{any}', function () {
        return view('app');
    })->where('any', '^(?!api).*$');
});

//Route::group(['middleware' => ['web']], function () {
////    Route::any('/{any}', function () {
////        return view('app');
////    })->name('login');
//
////    Route::any('/', function () {
////        return view('app');
////    });
//
//    Route::any('/{any}', function () {
//        return view('app');
//    });
//});
